# Calculator FastAPI+HTML

## Description

This is a simple Python FastAPI calculator

## Prerequisites

- Python 3.11 or higher
- pip

## Clone the Repository

```bash
git clone https://github.com/drrest/calc.git
cd calc
```
 
## Installation

```bash
pip install -r requirements.txt
```

## Running project

```bash
uvicorn app:app --reload
```

