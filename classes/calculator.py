from abc import ABC

from simpleeval import simple_eval


class BaseCalculator(ABC):
    fields_optional = ['a', 'b']
    filter_params = ['op', 'formula']

    def __init__(self):
        self.result = None
        self.params = {}
        self.settings = None
        self.rules = []

    def before_calculations(self, data: dict) -> None:
        """
            It is a transit layer to understand request params
        :param data:
        :return:
        """
        pass

    def calculate(self):
        """
            It is a running of calculating pipeline based on our operations classes or simple_eval ...
        :return:
        """

    def after_calculations(self):
        """
            Rule based system
            As example:
                self.rules = [{"id": 1,
                       "name": "odd_or_even",
                       "result": lambda _: 'green' if _ % 2 == 0 else 'red',
                       "field_name": "color",
                       "format": "json"},
                      {"id": 2,
                       "name": "float_or_int",
                       "result": lambda _: 'int' if isinstance(_, int) else 'float',
                       "field_name": "type",
                       "format": "json"}, ...]

            Here is a examples of rulse to make results of odd or even and set add type of result (int/float)

            You can easily add additional rules to configure figureout
        :return:
        """


class Calculator(BaseCalculator):
    # We extend list of possible names of acceptable variables
    fields_optional = ['a', 'b', 'c', 'e']
    fields_operators = ['formula', 'op']

    def __init__(self, settings):
        super().__init__()
        self.params = {}
        self.settings = settings
        self.template = None
        self.result = None
        self.rules = [
            {"id": 1,
             "name": "odd_or_even",
             "result": lambda _: 'green' if _ % 2 == 0 else 'red',
             "field_name": "odd_or_even_content",
             "field_format": "html"},

            {"id": 2,
             "name": "float_or_int",
             "result": lambda _: 'int' if isinstance(_, int) else 'float',
             "field_name": "type",
             "field_format": "text"},

            {"id": 3,
             "name": "if_error",
             "result": lambda _: f'Error: {_}' if isinstance(_, dict) else None,
             "field_name": "error",
             "field_format": "text",
             "field_optional": True}
        ]

    def reset_calc(self):
        self.template = None
        self.result = None
        self.params = {}

    def before_calculations(self, data: dict) -> dict[str, str | int] | dict[str, str | int] | bool | int:

        if "formula" not in data and "op" not in data:
            return {
                "error_code": 7001,
                "error": "Unsupported operations. Required 'formula' field or 'op' with one of values['+','-','*','/']"
            }

        if "formula" in data and data["formula"] is not None:
            self.params['formula'] = data['formula']
        else:
            self.params['op'] = data['op']

        for operand in data:
            if operand not in self.fields_optional + self.fields_operators:
                return {
                    "error_code": 7002,
                    "error": f"Parameter {operand} is not allowed. Allowed list: {self.fields_optional}"
                }
            else:
                if isinstance(data[operand], int) or isinstance(data[operand], float):
                    self.params[operand] = data[operand]

        return True

    def calculate(self):
        if 'formula' in self.params and self.params['formula'] is not None:
            # Here is the simplest way to solve calculation based on simple_eval - it is more safe version of eval()
            try:
                self.result = simple_eval(
                    self.params['formula'],
                    names={_: self.params[_] for _ in self.fields_optional if _ in self.params}
                )

                return self.result

            except ZeroDivisionError:
                """
                    This exception raised from div.py 
                """
                return {
                    "error_code": 7003,
                    "error": "Division by zero."
                }
            except Exception as e:
                return {
                    "error_code": 7004,
                    "error": str(e)
                }

        elif 'op' in self.params:
            # But let's back to our process line
            try:
                _operation = self.params['op']
                _operation_class = self.settings.ops[_operation]
                _operands = [self.params[_] for _ in self.params if _ not in self.filter_params]

                if len(_operands) == 0:
                    return {
                        "error_code": 7007,
                        "error": "At least 1 operands need (to do nothing) :)"
                    }

                self.result = _operation_class.execute(_operands)

            except ZeroDivisionError:
                return {
                    "error_code": 7005,
                    "error": "Division by zero."
                }
            except Exception as e:
                return {
                    "error_code": 7006,
                    "error": str(e)
                }

    def after_calculations(self):
        # If no rules to figureout - return ASIS
        if self.result and len(self.rules) == 0 and not self.template:
            return self.result

        self.template = {
            "result": self.result
        }

        # Will proceed each rule
        for rule in self.rules:

            if rule['field_format'] == "html":
                if self.result:
                    self.template[rule[
                        "field_name"]] = f"<span style='font-size:20pt;color:{rule['result'](self.result)}'>{self.result}</span>"

            elif rule['field_format'] == "text":

                _rule_result = rule["result"](self.result)
                if 'field_optional' not in rule or not rule['field_optional'] or _rule_result:
                    self.template[rule["field_name"]] = _rule_result
