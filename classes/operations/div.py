from functools import reduce
import operator
from classes.operations import Operation


class Divide(Operation):
    human_item = "/"
    priority = 4

    def execute(self, args):
        if not all(args) > 0:
            raise ZeroDivisionError("Division by zero is not allowed. One or more elements is 0")
        return reduce(operator.truediv, args)
