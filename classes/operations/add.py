from classes.operations import Operation


class Add(Operation):
    human_item = "+"
    priority = 3

    def execute(self, args):
        return sum(args)
