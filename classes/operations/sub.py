from functools import reduce
import operator
from classes.operations import Operation


class Subtract(Operation):
    human_item = "-"
    priority = 2

    def execute(self, args):
        return reduce(operator.sub, args)
