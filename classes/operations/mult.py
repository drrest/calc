from functools import reduce
import operator
from classes.operations import Operation


class Multiply(Operation):
    human_item = "*"
    priority = 1

    def execute(self, args):
        return reduce(operator.mul, args)
