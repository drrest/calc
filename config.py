import importlib
import os

from logzero import logger
from classes.operations import Operation


class Settings:
    class_operations_folder = "./classes/operations"
    class_operations_rel = class_operations_folder[2:].replace("/", ".")
    ops = {}

    def __init__(self):
        self.load_modules()

    @classmethod
    def load_modules(cls):
        for filename in os.listdir(cls.class_operations_folder):
            if filename.endswith('.py') and filename != '__init__.py':
                module_name = filename[:-3]
                module = importlib.import_module(f"{cls.class_operations_rel}.{module_name}")
                logger.info(f"Module {module_name} was loaded ...")
                for item_name in dir(module):
                    item = getattr(module, item_name)
                    if isinstance(item, type) and issubclass(item, Operation) and item is not Operation:
                        cls.ops[f"{item.human_item}"] = item()

