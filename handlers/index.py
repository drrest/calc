from fastapi import APIRouter
from starlette.requests import Request
from starlette.templating import Jinja2Templates

templates = Jinja2Templates(directory="templates")

router = APIRouter()


@router.get("/")
def index(request: Request):
    return templates.TemplateResponse("app.html", {"request": request})
