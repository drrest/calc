from typing import Optional, Any

from fastapi import APIRouter, HTTPException
from pydantic import create_model
from starlette.responses import JSONResponse

from classes.calculator import Calculator
from config import Settings

settings = Settings()
calculator = Calculator(settings=settings)

router = APIRouter()

# It will create Dynamically model from fields from Calculator class defined...
# In this case Model will be Here, othterwise -> /models/<mdl>.py ...
field_definitions = {field: (Optional[Any], None) for field in calculator.fields_optional + calculator.fields_operators}
CalculateData = create_model('CalculateData', **field_definitions)


@router.post("/evaluate")
def calculate(data: CalculateData):
    # Reset Calculator
    calculator.reset_calc()

    _bc_result = calculator.before_calculations(data.dict())

    if isinstance(_bc_result, dict):
        raise HTTPException(status_code=400, detail=_bc_result)

    _c_result = calculator.calculate()

    if isinstance(_c_result, dict):
        return HTTPException(status_code=400, detail=_c_result)

    _ac_result = calculator.after_calculations()

    if calculator.template:
        return JSONResponse(content={"result": calculator.template})
    else:
        return JSONResponse(content={"result": calculator.result})


@router.get("/settings")
def get_possible_operands():
    result = {
        "operators": list(settings.ops.keys()),
        "operands": calculator.fields_optional
    }
    return JSONResponse(content={"result": result})
