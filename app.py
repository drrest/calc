from fastapi import FastAPI
from starlette.staticfiles import StaticFiles

from handlers import index
from handlers import calculator

app = FastAPI()

app.mount("/fonts", StaticFiles(directory="./templates/fonts"), name="fonts")

app.include_router(index.router)
app.include_router(calculator.router, prefix="/calc")
